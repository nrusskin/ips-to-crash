

const load = async filePath => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader;
        reader.addEventListener('error', reject);
        reader.addEventListener("load", () => {
            const payload = reader.result.replace(/(?:\r\n|\r|\n)/g, '').replace(/}{/, '},{');
            resolve(JSON.parse(`[${payload}]`)[1]);
        });
        reader.readAsText(filePath, 'utf8');
    });
}

const formatFrame = (frame, idx, images) => {
    const {imageOffset, sourceLine, sourceFile, symbol, imageIndex, symbolLocation} = frame;
    const offset = ('000000000'+(imageOffset+images[imageIndex].base).toString(16)).substr(-16);
    const location = sourceFile ? `(${sourceFile}:${sourceLine})` : '';
    return `${idx}\t 0x${offset} ${images[imageIndex].name}\t${symbol} + ${symbolLocation} ${location}\n`;
}

const formatTread = (thread, idx, images) => {
    printLine(`<div id="${idx}">`)
        printLine(`<b>Thread ${idx}:</b>`);
        const frames = thread.frames.map((frame, idx) => formatFrame(frame, idx, images));
        printLine(`<plaintext>${frames.join('')}`);
    printLine(`</div>`)
}

const printHeader = content => {
    const {
        pid, procName, procPath, bundleInfo = {},
        cpuType, 
        parentProc, parentPid,
        userID,
        captureTime,
        osVersion,
        crashReporterKey,
        uptime, sip,
        exception,
        faultingThread,
    } = content;
    printLine(`<plaintext>
Process:               ${procName} [${pid}]
Path:                  ${procPath}
Identifier:            ${bundleInfo.CFBundleIdentifier}
Version:               ${bundleInfo.CFBundleShortVersionString}
Code Type:             ${cpuType}
Parent Process:        ${parentProc} [${parentPid}]
User ID:               ${userID}

Date/Time:             ${captureTime}
OS Version:            ${osVersion.train} (${osVersion.build})
Report Version:        
Anonymous UUID:        ${crashReporterKey}

Time Awake Since Boot: ${uptime} seconds

System Integrity Protection: ${sip}

Crashed Thread:        ${faultingThread}

Exception Type:        ${exception.type} (${exception.signal})
Exception Codes:       ${exception.codes}
Exception Note:        
`);

   printLine(`<b><a href="#${faultingThread}">Crashed Thread: ${faultingThread}</a></b>`);
}

const printFile = async (filePath) => {
    console.debug(`Analyzing ${filePath}`);
    const hideSystemThreads = document.getElementById('hide-system-threads').checked;
    const content = await load(filePath); 
    printHeader(content)
    content.threads.forEach((thread, idx) => {
        const systemLibraries = ['libsystem_kernel.dylib', 'libsystem_pthread.dylib'];
        if (hideSystemThreads && idx != content.faultingThread &&
            thread.frames.length && systemLibraries.includes(content.usedImages[thread.frames[0].imageIndex].name))
            return;
        formatTread(thread, idx, content.usedImages)
    });
}

const printFiles = (files) => {
    clearResult();
    if (files.length) {
        printFile(files[0]); // TODO: print first file only
    }
}

const printLine = line => {
    const el = document.createElement("span");
    el.innerHTML = line;
    document.getElementById('result').appendChild(el);
}

const clearResult = () => {
    document.getElementById('result').innerHTML = '';
}


window.addEventListener("load", () => {
    const fileTarget = document.getElementById('file');
    fileTarget.addEventListener('change', (event) => {
        printFiles(event.target.files);
    });

    document.getElementById('hide-system-threads').addEventListener('change', () => {
            printFiles(fileTarget.files);
      });
});
